module.exports = (ProductRepository) => {

    async function Execute(productId) {
        return ProductRepository.getById(productId);
    }

    return {
        Execute
    };
};
