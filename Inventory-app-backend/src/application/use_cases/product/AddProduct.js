const Product = require('../../../domain/Product');

module.exports = (ProductRepository, CrmServices) => {

    async function Execute(name, description, reference) {
        //const student = await StudentRepository.getByEmail(email);

        // validate
        if (!name || !description || !reference) {
            throw new Error('validation failed');
        }
        // check if student exist by email
        //if (student) {
        //    throw new Error('email already exist in the system');
        //}

        // create new student object
        let newProduct = new Product(name, description, reference);

        // persist product
        newStudent = await ProductRepository.add(newProduct)
        // notify crm system
        //await CrmServices.notify(newStudent);

        return 'product added successfully';
    }
    return {
        Execute
    };
};
