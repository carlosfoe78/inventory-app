module.exports = class Inventory {
    constructor(unit,availableQuantity, unitCost, initialQuantity,minQuantity,warehouses) {
        this.unit = unit;
        this.availableQuantity=availableQuantity;
        this.unitCost = unitCost;
        this.initialQuantity = initialQuantity;
        this.minQuantity= minQuantity
        this.warehouses = warehouses
    }
};
