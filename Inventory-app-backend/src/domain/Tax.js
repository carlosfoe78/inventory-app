module.exports = class Tax {
    constructor(name, percentage, description,status) {
        this.id = null;
        this.name = name;
        this.percentage = percentage;
        this.description = description;
        this.status = status;
    }
};
