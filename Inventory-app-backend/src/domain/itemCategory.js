module.exports = class ItemCategory {
    constructor(name,description,status) {
        this.id = null;
        this.name = name;
        this.description = description;
        this.status = status;
    }
};
