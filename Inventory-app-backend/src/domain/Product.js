module.exports = class Product {
    constructor(name, description, reference,status, itemCategory, category, inventory,tax, price, customFields) {
        this.id = null;
        this.name = name;
        this.description = description;
        this.reference= reference;
        this.status= status;
        this.itemCategory = itemCategory;
        this.category= category;
        this.inventory= inventory;
        this.tax = tax;
        this.price= price;
        this.customFields= customFields;
    }
};
