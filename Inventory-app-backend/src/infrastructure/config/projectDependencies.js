const UniversityCrmServices = require('../externalServices/UniversityCrmServices');
const MongoDatabaseServices = require('../persistance/mongodb/MongoDBServices')

module.exports = (() => {
    return {
        MongoDatabaseService: new MongoDatabaseServices(),
        CrmServices: new UniversityCrmServices()
    };
})();
