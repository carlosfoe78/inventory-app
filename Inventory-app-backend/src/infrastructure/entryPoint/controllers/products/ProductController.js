const AddProduct = require('../../../../application/use_cases/product/AddProduct');
const GetAllProduct = require('../../../../application/use_cases/product/GetAllProducts');
const GetProduct = require('../../../../application/use_cases/product/GetProduct');


module.exports = (dependecies) => {
    const { ProductRepository } = dependecies.MongoDatabaseService;
    const { CrmServices } = dependecies;

    const addNewProduct = (req, res, next) => {
        // init use case
        const AddProductCommand = AddProduct(ProductRepository, CrmServices);
        // extract student properties
        const { name, description, reference } = req.body;
        // call use case
        AddProductCommand.Execute(name, description, reference).then((response) => {
            res.json(response);
        }, (err) => {
            next(err);
        });
    };

    const getAllProducts = (req, res, next) => {
        // init use case
        const GetAllProductsQuery = GetAllProduct(ProductRepository);

        GetAllProductsQuery.Execute().then((students) => {
            res.json(students);
        }, (err) => {
            next(err);
        });
    };

    const getProduct = (req, res, next) => {
        const GetProductQuery = GetProduct(productRepository);

        GetProductQuery.Execute(req.params.studentId).then((student) => {
            res.json(student);
        }, (err) => {
            next(err);
        });
    };

    return {
        addNewProduct,
        getAllProducts,
        getProduct
    };
};
