const express = require('express');
const products = require('./products');

const apiRouter = (dependencies) => {
    const routes = express.Router();
    const productsRouter = products(dependencies);

    routes.use('/products', productsRouter);
    return routes;

};

module.exports = apiRouter;
