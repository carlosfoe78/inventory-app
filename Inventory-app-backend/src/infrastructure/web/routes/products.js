const express = require('express');
const ProductController = require('../../entryPoint/controllers/products/ProductController');

// address - api/products
// load dependencies
const studentsRouter = (dependencies) => {
    const router = express.Router();

    // load controller with dependencies
    const controller = ProductController(dependencies);

    router.route('/')
        .get(controller.getAllProducts)
        .post(controller.addNewProduct);
    router.route('/:studentId')
        .get(controller.getProduct);
    /*router.route('/enrollment/:studentId')
        .post(controller.addEnrollment);*/
    return router;
};


module.exports = studentsRouter;
