'use strict';

const DatabaseServices = require('../../../application/contracts/DatabaseServices');
const ProductRepository = require('./MongoProductRepository')
const environment = require('../../config/environment');

const MongoClient = require('mongodb').MongoClient;

module.exports = class MongoDBServices extends DatabaseServices {
  	constructor() {
    	super();
    	this.ProductRepository = new ProductRepository();
		this.database = null 
  	}

  	async initDatabase() {
		try{
			const client = new MongoClient(environment.database.mongo_url)
			await client.connect();

			this.database = await client.db(environment.database.mongo_dbname);
			console.log('connected to MongoDB database!');

			await this.registerCollections();
		}
		catch (e){
			console.error(e)
		}
  	}

	async registerCollections(){
		this.ProductRepository.collection = this.database.collection("Product")
	}
};
