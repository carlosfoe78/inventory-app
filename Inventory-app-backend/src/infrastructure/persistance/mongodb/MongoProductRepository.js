/* eslint-disable no-param-reassign */
/* eslint-disable arrow-body-style */
const ProductRepository = require('../../../application/contracts/ProductRepository');

module.exports = class MongoProductRepository extends ProductRepository {

    constructor() {
        super();
        this.products = [];
        this.currentId = 1;
        
    }

    async add(instance) {
        try {
            await this.collection.insertOne(instance)
        } catch (error) {
            throw new Error('Error Occurred');
        }
        return instance;
    }

    async update(instance) {
        let student;
        try {
            student = this.students.find(x => x.id === instance.id);
            if (student) {
                Object.assign(student, { instance });
            }

        } catch (error) {
            throw new Error('Error Occurred');
        }

        return student;
    }

    async delete(studentId) {
        try {
            const studentIndex = this.students.findIndex(x => x.id === studentId);
            if (studentIndex !== -1) {
                this.students.splice(studentIndex, 1);
            }
        } catch (error) {
            throw new Error('Error Occurred');
        }

        return true;
    }

    async getById(studentId) {
        let student;
        try {
            const id = parseInt(studentId);
            student = this.students.find(x => x.id === id);
        } catch (err) {
            throw new Error('Error Occurred');
        }

        return student;
    }

    async getByEmail(studentEmail) {
        let student;
        try {
            student = this.students.find(x => x.email === studentEmail);
        } catch (err) {
            throw new Error('Error Occurred');
        }

        return student;
    }

    async getAll() {
        return this.students;
    }

    async addEnrollment(studentId, enrollment) {
        const id = parseInt(studentId);
        const student = this.students.find(x => x.id === id);

        if (!student) {
            throw new Error('student does not exist');
        }

        if (!student.enrollments) {
            student.enrollments = [];
        }
        student.enrollments.push(enrollment);

        return student;
    }
};
